import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { PhotoshootsComponent } from './photoshoots/photoshoots.component';
import { GalleriesComponent } from './galleries/galleries.component';
import { WinComponent } from './win/win.component';
import { AboutComponent } from './about/about.component';
import { MagazineComponent } from './magazine/magazine.component';
import { SponsorContestComponent } from './sponsor-contest/sponsor-contest.component';
import { PrivacyComponent } from './privacy/privacy.component';
import { TermsComponent } from './terms/terms.component';
import { SupportComponent } from './support/support.component';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';

const routes: Routes = [
	{
		path: '',
        component: HomeComponent	
	},
	{
		path: 'photo-shoots',
        component: PhotoshootsComponent	
	},
	{
		path: 'galleries',
        component: GalleriesComponent	
	},
	{
		path: 'win',
        component: WinComponent	
	},
	{
		path: 'about',
        component: AboutComponent	
	},
	{
		path: 'magazine',
        component: MagazineComponent	
	},
	{
		path: 'sponsor',
        component: SponsorContestComponent	
	},
	{
		path: 'privacy',
        component: PrivacyComponent	
	},
	{
		path: 'terms',
        component: TermsComponent	
	},
	{
		path: 'support',
        component: SupportComponent	
	},
	{
		path: '**',
        component: PagenotfoundComponent	
	}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
