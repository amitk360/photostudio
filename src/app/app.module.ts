import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { HomeComponent } from './home/home.component';
import { PhotoshootsComponent } from './photoshoots/photoshoots.component';
import { GalleriesComponent } from './galleries/galleries.component';
import { WinComponent } from './win/win.component';
import { AboutComponent } from './about/about.component';
import { MagazineComponent } from './magazine/magazine.component';
import { SponsorContestComponent } from './sponsor-contest/sponsor-contest.component';
import { PrivacyComponent } from './privacy/privacy.component';
import { TermsComponent } from './terms/terms.component';
import { SupportComponent } from './support/support.component';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    PhotoshootsComponent,
    GalleriesComponent,
    WinComponent,
    AboutComponent,
    MagazineComponent,
    SponsorContestComponent,
    PrivacyComponent,
    TermsComponent,
    SupportComponent,
    PagenotfoundComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
