import { Component, OnInit } from '@angular/core';
import { HostListener } from '@angular/core';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  showHeader = true;
  showUserlog = false;
  hidelogBtn = true;
  hideUserpDropdown = false;
  hideNotiDropdown = false;
  constructor() { }
	
  @HostListener('window:resize', ['$event'])
  onResize(event) {
	this.resizeWindow();
  }
  
  ngOnInit() {
	this.resizeWindow();
  }
  
  changeMenu(){
	  if (window.innerWidth < 991) {		  
			if (this.showHeader ) {
				this.showHeader = false;
			} else {
				this.showHeader = true;
			}
	  }	
  }
  
  resizeWindow() {
	  if (window.innerWidth < 991) {
		  this.showHeader = false;
		} else {
			this.showHeader = true;
		}
  }
  
  changeUserlog(){
	if(this.showUserlog){
		this.showUserlog = false;
		this.hidelogBtn = true;
	} else {
		this.showUserlog = true;
		this.hidelogBtn = false;
	}
  }
  
  changeUserProfileDropdown(){
	if(this.hideUserpDropdown){
		this.hideUserpDropdown = false;
		this.showUserlog = false;
		this.hidelogBtn = true;
	} else {
		this.hideUserpDropdown = true;	
		
	}
  }
  
  changeUserNotiDropdown(){
	if(this.hideNotiDropdown){
		this.hideNotiDropdown = false;
	} else {
		this.hideNotiDropdown = true;
	}
  }
  
}